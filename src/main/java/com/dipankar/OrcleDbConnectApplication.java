package com.dipankar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrcleDbConnectApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrcleDbConnectApplication.class, args);
	}

}
