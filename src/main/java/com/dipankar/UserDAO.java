package com.dipankar;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class UserDAO {
	
	@Autowired 
    private JdbcTemplate jdbcTemplate;
	
	public List<User> getUserInfo() {
		
		List<User> users = new ArrayList();
		
		jdbcTemplate.query(
                "select * from DM_user", 
                (rs, rowNum) -> new User(rs.getString("user_name"), rs.getString("password"), rs.getString("display_name"))
        ).forEach(user -> users.add(user));
		
		return users;
	}
	

}