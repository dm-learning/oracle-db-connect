package com.dipankar;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {
	
	@Autowired
	private UserDAO userDAO;
	
	@GetMapping(value = "/hello")
	public String sayHello() {
		return "Say Hello";
	}
	
	@GetMapping(value = "/user")
	public List getUserInfo() {
		return userDAO.getUserInfo();
		
	}

}
