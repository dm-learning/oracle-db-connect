Maven repository does not have Oracle JDBC driver. 

To add it 
1. Download the Oracle JDBC driver 
https://www.oracle.com/database/technologies/jdbc-drivers-12c-downloads.html

2. Add it to local maven repository 
mvn install:install-file -Dfile=ojdbc7.jar  -DgroupId=com.oracle -DartifactId=ojdbc7 -Dversion=12.1.0.1 -Dpackaging=jar

Note: If you have to not installed Maven in your workstation, you can use STS or Eclipse to add it. 
Right click on POM file -> Run As -> Maven build -> put 'install:install-file -Dfile=ojdbc7.jar  -DgroupId=com.oracle -DartifactId=ojdbc7 -Dversion=12.1.0.1 -Dpackaging=jar' in Goals section
Befor that you need to put the downloaded in the jar file in Project root folder. 
/Users/dipankar/Professional/Learning/workspace/OrcleDBConnect

3. Use the following maven dependency in project POM file 
        <dependency>
            <groupId>com.oracle</groupId>
            <artifactId>ojdbc7</artifactId>
            <version>12.1.0.1</version>
        </dependency>


Configuring datasource

oracle.url=jdbc:oracle:thin:@//localhost:1521/ORCLCDB.localdomain
Here ORCLCDB.localdomain is the service name. It is not SID 

Docker Setup

Start oracle 

docker exec -it 836c9eb850a1 bash -c "source /home/oracle/.bashrc; sqlplus /nolog"

Connect to SQL

connect sys as sysdba

alter session set “_ORACLE_SCRIPT” = true;

connect dummy/dummy
show user

stop oracle service

docker ps -a

docker container stop <containerid>

docker contaner start<containerid>

